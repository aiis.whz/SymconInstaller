<?
/**
 * WindNODE-Installer Klasse zur Installation von projektspezifischen IP-Symcon-Komponenten.
 * Enthält den Namespace WNI.
 * Erweitert IPSModule.
 * 
 * @package       WindNODE-Installer
 * @author        Marek Kretzschmar <marekre@fh-zwickau.de>
 * @copyright     2018-2019 Westsächsische Hochschule Zwickau
 * @license       
 * @version       0.2 
 * @example <b>Ohne</b>
 * @property int $FrameID
 * @property array $ReplyData
 * @property string $BufferIN
 */
class WNInstaller extends IPSModule {
 
    /**
     * Interne Funktion des SDK.
     *
     * @access public
     */
    public function __construct($InstanceID) {
        parent::__construct($InstanceID);
    }
 
    /**
     * Interne Funktion des SDK.
     *
     * @access public
     */
    public function Create() {
        parent::Create();
    }
 
    /**
     * Interne Funktion des SDK.
     *
     * @access public
     */
    public function ApplyChanges() {
        parent::ApplyChanges();
    }
 
    /**
     * WNI_Install($id);
     *
     */
    public function Install() {
        $hostname = gethostname();
        echo "Installiere WindNODE Komponenten für Instanz {$hostname}";
        
        $allowed_hosts = array('WiNoGate01' => 1, 'WiNoGate02' => 4);

        // Validate $hostname
        if(!array_key_exists($hostname, $allowed_hosts)) {
	        echo "Falsches Zielsystem. Installation wird beendet.\n";
        } else {
            $this->executeInstall($hostname);
        }


    }

    /**
     * Interne Installer-Funktion
     *
     */
    private function executeInstall($hostname) {
        // Rename ROOT Category
	    IPS_SetName(0, $hostname);
    }
}

?>