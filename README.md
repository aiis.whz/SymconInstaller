<a href="https://www.symcon.de"><img src="https://img.shields.io/badge/IP--Symcon-4.0-blue.svg?style=flat-square"/></a>
<a href="https://www.symcon.de"><img src="https://img.shields.io/badge/IP--Symcon-5.0-blue.svg?style=flat-square"/></a>

## Installer für AIIS-spezifische IP-Symcon Module

| UUID | Typ | Name | Bemerkung |
| ---- | --- | ---- | ---- |
| {57A6FBE4-DAC1-4F13-A1B2-3241AA54EE0C} | Bibliothek | Library            | |
| {700423C6-3956-40D1-A00A-512C8200EFF7} | Modul      | WindNODE-Installer | Installer für WiNoGate / WiNoFlatGate |
| {1DF7C133-F9EA-42A0-9923-B0D5F10BE8EC} | Modul      | HKV-Installer      | Installer für Metrona HKV/Datensammler |
| {75F98A3A-9564-4681-A358-8C7F544F5564} | Modul      |                    | |
| {727D4689-2E40-44C4-B02F-7047D1DA4835} | Modul      |                    | |


Dieses Module kann über die Modulverwaltung oder ab Version 5.1 über den
Module-Store installiert werden.Es muss lediglich eine Instanz angelegt werden,
welche dann eine einfache Installation und Konfiguration verschiedener
Geräteinstanzen ermöglicht (teilautomatisierte Installation und Konfiguration).


