<?
/**
 * HKV-Installer Klasse zur Installation von projektspezifischen IP-Symcon-Komponenten.
 * Enthält den Namespace HKV.
 * 
 * Installiert die klassische HVK Datenerfassungkette (siehe auch https://gitlab.com/symcon/symcondevop/tree/master/HKV1.0)
 * 
 * Client Socket -> Cutter -> RegisterVariable -> Script
 *
 * Aktueller Stand: - manuelles Anlegen der oben dargestellten Kette durch Angabe der Parameter
 * 
 * - DSID   -> ID des Datensammlers, kann frei gewählt werden, gegenwärtig empfohlen: 'DS<Straßenkürzel><Hausnummer>'
 * - DSName -> Bezeichnung für den Datensammler
 * - Host   -> Hostname oder IP-Adresse (IPv4)
 * - Port   -> default 24270 
 *
 * @package       HKV-Installer
 * @author        Marek Kretzschmar <marekre@fh-zwickau.de>
 * @copyright     2019 Westsächsische Hochschule Zwickau
 * @license       
 * @version       1.1
 */
class HKVInstaller extends IPSModule {
 
    /**
     * Interne Funktion des SDK.
     *
     * @access public
     */
    public function __construct($InstanceID) {
        parent::__construct($InstanceID);
    }
 
    /**
     * Interne Funktion des SDK.
     *
     * @access public
     */
    public function Create() {
        parent::Create();
    }
 
    /**
     * Interne Funktion des SDK.
     *
     * @access public
     */
    public function ApplyChanges() {
        parent::ApplyChanges();
    }
 
    /**
     * HKV_Install($id);
     *
     */
    public function Install() {
        $hostname = gethostname();
        echo "Installiere Komponenten für Host {$hostname}.\n\n";
        
        // Rename ROOT Category
	    IPS_SetName(0, $hostname);
        $this->executeInstall($hostname);
    }

    /**
     * Interne Installer-Funktion
     *
     */
    private function executeInstall($hostname) {
        echo "Nicht implementiert. Derzeit kann nur eine einzelne Instanz manuell angelegt werden.";
    }
    
    /**
     * Externe Funktion HKV_CreateDSInstance($id, $DSID, $DSName, $DSHost, $DSPort)
     *
     */
    public function CreateDSInstance($DSID, $DSName, $DSHost, $DSPort) {

        //Validierung/Default Values
        if($DSPort==="") {
            $DSPort = 24270;
        }

        echo "\n\nLege neuen Datensammler an:\n DSID = $DSID\n Name = $DSName\n Host = $DSHost\n Port=$DSPort\n";

        $categoryID = $this->createCategory(0, "HKV");
        $varID = $this->createVariables($categoryID, $DSID);
        $scriptID = $this->createScripts($categoryID);
        $socketID = $this->createClientSocket($DSID, $DSHost, $DSPort);
        $cutter = $this->createCutter($DSID, $socketID);
        $regVarID = $this->createRegisterVariable($categoryID, $scriptID, $DSID, $cutter);
        echo "\n\nFertig!";
    }
    
    /**
     * Kategorie unterhalb der Root-Kategorie (0) anlegen
     * S.a. https://www.symcon.de/service/dokumentation/befehlsreferenz/kategorieverwaltung/ips-createcategory/
     */
    private function createCategory($rootID, $name) {

        // Ist die Kategorie bereits angelegt?
        $CatID = @IPS_GetCategoryIDByName($name, 0);
        if ($CatID === false)
        {
            $CatID = IPS_CreateCategory();
            IPS_SetName($CatID, $name);
            IPS_SetParent($CatID, 0);
            echo "\nKategorie $name ($CatID) angelegt...\n";
        }
        else
            echo "\nKategorie $name bereits vorhanden ($CatID), wird benutzt...\n";

        return $CatID;
    }

    /**
     * Anlegen der Variable HKVMESSAGES (Typ String) unterhalb der Kategorie $categoryID
     * und Aktivierung des Loggings
     */
    private function createVariables($categoryID, $DSID) {

        $archiveID = IPS_GetInstanceListByModuleID("{43192F0B-135B-4CE7-A0A7-1475603F3060}")[0];

        // Ist die Variable HKVMESSAGES schon angelegt?
        $VarID = @IPS_GetVariableIDByName("HKVMESSAGES", $categoryID);
        if ($VarID === false)
        {
            $VarID = IPS_CreateVariable(3); // 0..Boolean, 1..Integer, 2..Float, 3..String
            IPS_SetName($VarID, "HKVMESSAGES");
            IPS_SetParent($VarID, $categoryID);
            // Logging aktivieren
            AC_SetLoggingStatus($archiveID, $VarID, true); 
            IPS_ApplyChanges($archiveID); 
            echo "\nVariable HKVMESSAGES ($VarID) angelegt und Logging aktiviert...\n";
        }
        else
            echo "\nVariable HKVMESSAGES bereits vorhanden ($VarID), wird benutzt...\n";

    	$CounterID = @IPS_GetVariableIDByName("MsgCounter_".$DSID, $categoryID);
        if ($CounterID === false)
        {
            $CounterID = IPS_CreateVariable(1); // 0..Boolean, 1..Integer, 2..Float, 3..String
            IPS_SetName($CounterID, "MsgCounter_".$DSID);
            IPS_SetParent($CounterID, $categoryID);
            // Logging aktivieren
            AC_SetLoggingStatus($archiveID, $CounterID, true); 
            IPS_ApplyChanges($archiveID); 
            echo "\nVariable MsgCounter_$DSID ($CounterID) angelegt und Logging aktiviert...\n";
        }
        else
            echo "\nVariable MsgCounter_$DSID bereits vorhanden ($CounterID), wird benutzt...\n";

        return $CounterID;
    }

    /**
     *
     */
    private function createScripts($categoryID) {

        // Ist vielleicht schon ein HKV_RECEIVER Skript angelegt?
        $ScriptID = @IPS_GetScriptIDByName("HKV_RECEIVER", $categoryID);
        if ($ScriptID === false)
        {
            $ScriptID = IPS_CreateScript(0);
            IPS_SetName($ScriptID, "HKV_RECEIVER");
            IPS_SetParent($ScriptID, $categoryID);
            IPS_SetScriptContent($ScriptID, $this->getScriptContent($categoryID));
            echo "\nScript HKV_RECEIVER angelegt...\n";
        }
        else
            echo "\nScript HKV_RECEIVER bereits vorhanden ($ScriptID), wird benutzt...\n";

        return $ScriptID;
    }

    /**
     *
     */
    private function createClientSocket($DSID, $DSHost, $DSPort) {
        IPS_LogMessage ("HKV-Installer", "createClientSocket($DSHost, $DSPort)");
        //Cutter erstellen
        $socketID = IPS_CreateInstance("{3CFF0FD9-E306-41DB-9B5A-9D06D38576C3}");
 
        IPS_SetName($socketID, "Client Socket ".$DSID); // Instanz benennen
        //IPS_SetParent($cutterID, 12345); // bei Cutter nicht notwendig
 
        //Konfiguration {"Open":true,"Host":"192.168.11.20","Port":24270}
        IPS_SetProperty($socketID, "Open", 0);
        IPS_SetProperty($socketID, "Host", $DSHost);
        IPS_SetProperty($socketID, "Port", $DSPort);
        IPS_ApplyChanges($socketID);

        echo "\nClient Socket ($socketID) angelegt und konfiguriert.\n";
        return $socketID;
    }
    
    /**
     *
     */
    private function createCutter($DSID, $socketID) {
        IPS_LogMessage ("HKV-Installer", "createCutter($DSID, $socketID)");

        //Cutter erstellen
        $cutterID = IPS_CreateInstance("{AC6C6E74-C797-40B3-BA82-F135D941D1A2}");
 
        IPS_SetName($cutterID, "Cutter ".$DSID); // Instanz benennen
        //IPS_SetParent($cutterID, 12345); // bei Cutter nicht notwendig
 
        //Konfiguration {"ParseType":1,"LeftCutChar":"","RightCutChar":"","DeleteCutChars":false,"InputLength":61,"SyncChar":"1B","Timeout":0}
        IPS_SetProperty($cutterID, "ParseType", 1);
        IPS_SetProperty($cutterID, "InputLength", 61);
        IPS_SetProperty($cutterID, "SyncChar", "1B");
        IPS_ApplyChanges($cutterID);

        //mit Client Socket verknüpfen -> [ConnectionID]
        IPS_ConnectInstance($cutterID, $socketID);

        echo "\nCutter ($cutterID) angelegt, konfiguriert und mit Client Socket verknüpft.\n";
        return $cutterID;
    }

    /**
     *
     */
    private function createRegisterVariable($categoryID, $scriptID, $DSID, $cutterID) {
        IPS_LogMessage ("HKV-Installer", "createRegisterVariable($categoryID, $scriptID, $DSID, $cutterID)");
        //$ds = $this->RegisterVariableInteger("DSHH18", "Datensammler HH18", "", 18);
        //$ds = $this->RegisterVariableInteger("DSHH20", "Datensammler HH20", "", 20);
        //$ds = $this->RegisterVariableInteger("DSHH21", "Datensammler HH21", "", 21);
        //$ds = $this->RegisterVariableInteger("DSHH22", "Datensammler HH22", "", 22);
        //$ds = $this->RegisterVariableInteger("DSHH23", "Datensammler HH23", "", 23);
        //Statusvariable
        //$ds = $this->RegisterVariableInteger($DSID, "Datensammler ".$DSID, "", 0);
        
        //RegisterVariable erstellen
        $regVarID = IPS_CreateInstance("{F3855B3C-7CD6-47CA-97AB-E66D346C037F}");
 
        IPS_SetName($regVarID, $DSID); // Instanz benennen
        IPS_SetParent($regVarID, $categoryID);
 
        //Konfiguration {"RXObjectID":44380}
        IPS_SetProperty($regVarID, "RXObjectID", $scriptID);
        //IPS_SetConfiguration($regVarID, "{\"RXObjectID\":$scriptID");
        IPS_ApplyChanges($regVarID);

        //mit Cutter verknüpfen -> [ConnectionID]
        IPS_ConnectInstance($regVarID, $cutterID);
        echo "\nRegisterVariable ($regVarID) angelegt, konfiguriert und mit Cutter verknüpft.\n";
        return $regVarID;
    }

    /**
     *
     */
    private function getScriptContent($categoryID) {

        $data = "PD8KLyoqCiAqID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09CiAqIEhLViBSZWNlaXZlciBTY3JpcHQgZm9yIE1ldHJvbmEgV2lyZWxlc3MgTWVzc2FnZXMKICoKICogKEMpIDIwMTQtMjAxOSBVbml2ZXJzaXR5IG9mIEFwcGxpZWQgU2NpZW5jZXMgWndpY2thdQogKgogKiB2MS4yCiAqIAogKiBAbWFyZWtyZQogKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PQogKi8KCmlmICgkX0lQU1snU0VOREVSJ10gPT0gIlJlZ2lzdGVyVmFyaWFibGUiKQp7CgoJJGRhdGEgID0gJF9JUFNbJ1ZBTFVFJ107Cgkkc3JjID0gSVBTX0dldE5hbWUoJF9JUFNbJ0lOU1RBTkNFJ10pOwoJCglJUFNfTG9nTWVzc2FnZSgkX0lQU1snSU5TVEFOQ0UnXS4iICgiLiRzcmMuIikiLCAkX0lQU1snU0VOREVSJ10uJyBIS1YtTWVzc2FnZTogJy4kZGF0YSk7CgkKICAgICRQYXJlbnRJRCA9ICUlUEFSRU5UX0lEJSU7CiAgICAKIAkkTWVzc2FnZXNWYXJJRCA9IElQU19HZXRWYXJpYWJsZUlEQnlOYW1lKCJIS1ZNRVNTQUdFUyIsICRQYXJlbnRJRCk7CglTZXRWYWx1ZSgkTWVzc2FnZXNWYXJJRCwgJGRhdGEpOwogCglwZXJzaXN0TWVzc2FnZSgkZGF0YSwgJHNyYyk7CgoJLy8gTmFjaHJpY2h0ZW56w6RobGVyIGbDvHIgZGVuIERhdGVuc2FtbWxlciBlcmjDtmhlbgoJJENvdW50ZXJJRCA9IElQU19HZXRWYXJpYWJsZUlEQnlOYW1lKCJNc2dDb3VudGVyXyIuJHNyYywgJFBhcmVudElEKTsKCVNldFZhbHVlKCRDb3VudGVySUQgLCBHZXRWYWx1ZSgkQ291bnRlcklEKSArIDEpOwp9CgovKioKICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0KICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0KICovCmZ1bmN0aW9uIHBlcnNpc3RNZXNzYWdlKCRtZXNzYWdlLCAkc3JjKSB7CiAgCiAgLy8gcGVyc2lzdCB0byBleHRlcm5hbCBkYXRhYmFzZQogIHBlcnNpc3RNeVNRTCgkbWVzc2FnZSwgJHNyYyk7CiAgCn0KCi8qKi8KZnVuY3Rpb24gcGVyc2lzdE15U1FMKCRtZXNzYWdlLCAkc3JjKSB7CgoJJG15c3FsaSA9IG15c3FsaV9jb25uZWN0KCdkYXRhY2VudGVyLnNlY3VyZS5naWl6JywgJ2dpaXpfd3JpdGVyJywgJyRHaWl6LjIwMTIhTXlTUUwnLCAnZ2lpeicpOwoKCQoJaWYgKCEkbXlzcWxpKSB7CgkJZGllKCdWZXJiaW5kdW5nIHNjaGx1ZyBmZWhsOiAnIC4gbXlzcWxpX2Nvbm5lY3RfZXJyb3IoKSk7Cgl9CgkKCSR0cyA9IGRhdGUoIlktbS1kIEg6aTpzIik7CgoJJHF1ZXJ5ID0gIklOU0VSVCBJTlRPIGBnaWl6YC5gaGt2bWVzc2FnZXNgKGB0c2AsYGhrdm1lc3NhZ2VgLGBzb3VyY2VgLGBzcmNfbG9jYCxgc3JjX2RzaWRgKSAKCVZBTFVFUygnIi4kdHMuIicsJyIuJG1lc3NhZ2UuIicsJ0JPUk5BX0hIMTgtMjMnICwnQk9STkEnLCciLiRzcmMuIicpCglPTiBEVVBMSUNBVEUgS0VZIFVQREFURSB0cz0nIi4kdHMuIic7IjsKCQoJJHJlc3VsdCA9ICRteXNxbGktPnF1ZXJ5KCRxdWVyeSk7CgoJJG15c3FsaS0+Y2xvc2UoKTsKfQoKPz4=";

        $content = str_replace("%%PARENT_ID%%", $categoryID, base64_decode($data));

        return $content;
    }
}

?>